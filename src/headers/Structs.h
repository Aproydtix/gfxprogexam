#pragma once

#include "Shaders.h"
#include "Texture.h"

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>


//	 Container for pointers to shader and texture, 
//	 Different materials may contain different combinations of shaders/textures
struct Material
{
    Shader* shader;
    Texture* texture;
	glm::vec4 color = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	float specularity = 64;
};

//	Container with functions for position and rotation
struct Transform
{
    glm::vec3 position;
    glm::mat4 rotation;
	glm::vec3 scale;
    float speed;

    void rotate(float angle, glm::vec3 direction, double delta_time)
    {
        rotation = glm::rotate(rotation, glm::radians(angle) * (float) delta_time, direction);
    }
    void translate(glm::vec3 direction, double delta_time)
    {
        position += (direction * (float)delta_time * speed);
    }
	glm::vec3 eulerAngles()
	{
		return glm::eulerAngles(glm::toQuat(rotation));
	}

	glm::vec3 forward()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(0, 0, 1), 1.0));
	}
	glm::vec3 up()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(0, 1, 0), 1.0));
	}
	glm::vec3 right()
	{
		return glm::normalize(rotation * glm::vec4(glm::vec3(1, 0, 0), 1.0));
	}

	void pitch(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, right()) * glm::quat(eulerAngles()));
	}
	void roll(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, forward()) * glm::quat(eulerAngles()));
	}
	void yaw(float angle)
	{
		rotation = glm::toMat4(glm::angleAxis(angle, up()) * glm::quat(eulerAngles()));
	}
};

//	Container for different buffer objects
struct Buffer
{
    GLuint VAO;
    GLuint VBO[3];
};

//	Light containing a transform (position, rotation, scale), and color
struct Light
{
	Transform transform;
	glm::vec3 color = glm::vec3(1.0f);
};