#pragma once

#include "src/headers/GameObject.h"
#include "src/headers/ObjectLoader.h"

#include <vector>


//	ObjectHandler lists
extern std::vector<Light*> lights;
extern std::vector<GameObject*> gameObjects;
extern std::vector<Mesh*> meshes;
extern std::vector<Texture*> textures;
extern std::vector<Shader*> shaders;

//	Create/Load functions
Light* CreateLight(glm::vec3 pos, glm::vec3 color = glm::vec3(1.0f));
void InitGameObject(GameObject* go, Mesh* obj, Texture* tex, Shader* shade);
Mesh* LoadObject(std::string fileName);
Texture* LoadTexture(std::string fileName);
Shader* LoadShader(std::string fileName);

//	Update functions
void UpdateGameObjects();
void DrawGameObjects();
void HandleInput(GLFWwindow* window, int key, int scancode, int action, int mods);
