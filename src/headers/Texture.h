#pragma once

#include <string>
#include <GL/glew.h>
#include <SDL2_ttf/include/SDL_ttf.h>


class Texture
{
public:
    GLuint m_texture_number;	//	Texture ID
	int width;					//	Width
	int height;					//	Height
	int nrChannels;				//	Color channels

	SDL_Surface* message;		//	Text texture

    Texture();
    Texture(const char* path);

    int createTextureFromText(TTF_Font* font, std::string text, SDL_Color color);	//	Creates the text texture
};
