#pragma once

#include "GameObject.h"
#include "Enums.h"

#include <SDL2_ttf/include/SDL_ttf.h>


//	Class containing text to be displayed in-game
class TextBox : public GameObject
{
public:
    TTF_Font* font;			//	Textbox font
	std::string text;		//	Textbox text
	std::string prevText;	//	Previous text, used to update the textbox texture if text changes
	SDL_Color color;		//	Color of the text
	Allignment allignment;	//	Allignment of the text
	bool isUI = true;			//	Whether the textbox is meant to be drawn as UI

    TextBox(TTF_Font* fnt, std::string text, SDL_Color color, glm::vec3 pos, Allignment allign, Shader* shader);
	void update();
	void draw();
	void createText();		//	Creates the text mesh / texture for the textbox
};