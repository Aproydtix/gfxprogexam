#pragma once

#include "GameObject.h"


//	Class containing a heightMap, as well as various variables and functions to update said heightMap
class Terrain : public GameObject
{
public:
	bool changingSeasons = true;	//	Whether seasons should update
	bool changingDayTime = true;	//	Whether dayTime should update
	float seasonGoal = 0;			//	What season it should be moving towards
	float dayGoal = 0;				//	What time of day it should be moving towards

    Terrain();

    virtual void update();
	virtual void draw();
    virtual void input(int key, int scancode, int action, int mods);
};
