#pragma once

#include "GameObject.h"
#include "Enums.h"


//	Class for basic pick-up
class Balloon : public GameObject
{
public:
	bool isActive = true;			//	Whether the Balloon can be collected or not

	Balloon();
	virtual void update();
	virtual void draw();
};