#pragma once

#include "Structs.h"
#include "ObjectLoader.h"


//	Base class for GameObjects in the project
class GameObject
{
public:
    Transform transform;        //	Position, Rotation, Scale
	Mesh* mesh;					//	Vertices, Normals, TextCoords
	Mesh* prevMesh;				//	Pointer to previous mesh, used to update buffers if mesh changes
	Buffer* buffer;				//	Buffer, VAO, VBO
	Material material;			//	Shader, Texture, Color, Specularity

	GameObject(glm::vec3 pos = glm::vec3(0.0f), glm::mat4 rot = glm::mat4(1.0f), glm::vec3 scale = glm::vec3(1.0f));
	~GameObject();

	virtual void init(Mesh* obj, Texture* tex, Shader* shade) final;	//	Initializes the GameObject and adds it to the Handler to be updated and drawn every frame
	virtual void drawInternal(glm::mat4 view) final;					//	Internal drawing function used by all GameObjects, called by draw()
	virtual void setMeshBuffers() final;								//	Sets the mesh's buffers

    virtual void update();												//	Update function for the GameObject, called every frame
	virtual void draw();												//	Draw function for the GameObject, called every frame
    virtual void input(int key, int scancode, int action, int mods);	//	Input function, allows you to get input on any GameObject
};
