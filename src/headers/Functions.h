#pragma once


//	Functions to be run in main()
void Start();		//	Used to initialize the game
void Update();		//	Used to update various aspects of the game
void LateUpdate();	//	Happens after all GameObjects have run their Update() functions