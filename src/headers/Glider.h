#pragma once

#include "GameObject.h"


class Glider : public GameObject
{
public:
	bool physics = true;	//	Whether the glider uses physics-like movement or not

	glm::vec3 speed;		//	Physics velocity vector
	float engineSpeed;		//	Engine speed, used to accelerate the glider with physics, or just straight up speed without it
	float minSpeed = 0.0f;	//	Minimum engineSpeed
	float maxSpeed = 15.0f;	//	Maxmimum engineSpeed and physics velocity
	glm::vec3 startPos;		//	Starting position

    Glider();

    virtual void update();
    virtual void input(int key, int scancode, int action, int mods);
	void reset();
	void shoot();
};
