#pragma once


//	Enum for textbox allignment
enum Allignment
{
    center,
    left,
    right
};
//	Enum for camera modes
enum CameraMode
{
    freeCam = 0,
	staticCam = 1,
	orbitPositionCam = 2,
	trackingCam = 3,
    followCam = 4,
    firstPersonCam = 5,
    orbitTargetCam = 6,
};