#pragma once

#include "Camera.h"
#include "Terrain.h"
#include "Glider.h"
#include "TextBox.h"
#include "Balloon.h"
#include "Bullet.h"

#define PI 3.14159265358979323846264338327950288


//	Global data
extern double g_deltatime;
extern Camera* mainCamera;
extern glm::vec2 mousePos;
extern int lastX;
extern int lastY;

//	Lists of loaded things
extern std::vector<Light*> lights;
extern std::vector<GameObject*> gameObjects;
extern std::vector<Mesh*> meshes;
extern std::vector<Texture*> textures;
extern std::vector<Shader*> shaders;

//----------------------------------------------------------------------
//	Project specific variables
//----------------------------------------------------------------------

//	Tetxures
extern Texture* whiteTex;
extern Texture* gliderTex;
extern Texture* yellowTex;
extern Texture* heightTex;
extern Texture* waterMelonTex;
extern Texture* greyTex;

//	Shaders
extern Shader* terrainShader;
extern Shader* standardShader;
extern Shader* textShader;
extern Shader* BaWShader;

//	Meshes
extern Mesh* heightMesh;
extern Mesh* gliderMesh;
extern Mesh* sunMesh;
extern Mesh* planeMesh;
extern Mesh* bulletMesh;

//	Textboxes
extern TextBox* seasonText;
extern TextBox* dayText;
extern TextBox* speedText;
extern TextBox* cameraText;
extern TextBox* scoreText;

//	Terrain
extern Terrain* terrain;
extern bool hardLines; 
extern float season;
extern float min;
extern float max;
extern glm::vec4 waterColor;
extern glm::vec4 vegetationColor;
extern glm::vec4 mountainColor; 
extern glm::vec4 snowColor; 
extern float dayTime;

//	Other GameObjects
extern Glider* glider;
extern GameObject* water;
extern GameObject* sun;

//	Game variables
extern int score;
extern std::vector<Balloon*> balloons;
extern std::vector<Bullet*> bullets;