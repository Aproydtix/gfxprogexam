#pragma once

#include <GL/glew.h>
#include <glm/glm/glm.hpp>
#include <vector>
#include <iostream>
#include <string>


class Mesh 
{
public:
    std::vector <glm::vec3> vertices;	 //	Vertices
    std::vector <glm::vec3> normals;	 //	Normals
    std::vector <glm::vec2> texCoord;    //	Texture Coordinates
    std::string mtllib;				     //	

	Mesh() {}

    Mesh(std::vector <glm::vec3> vert, std::vector <glm::vec3> norm, std::vector <glm::vec2> text, std::string mtll)
    {
		vertices = vert;	//	Vertices
		normals = norm;		//	Normals
		texCoord = text;	//	Texture Coordinates
		mtllib = mtll;		//
    }
};
