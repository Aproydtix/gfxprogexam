#pragma once

#include "Mesh.hpp"

#include <string>


//	Represents faces
struct Triangle		
{
	glm::vec3 A;
	glm::vec3 B;
	glm::vec3 C;
};

//	Class to create a heightMap
class HeightMap
{
private:
	glm::vec3 FaceNormal(Triangle tri);											//	Calculates normal of face
	glm::vec3 VertexNormal(GLfloat* values, glm::vec2 &size, glm::vec2 &pos);	//	Calculates normal of vertex
	Mesh CreateMesh(GLfloat* heightValues, glm::vec2 size);						//	Creates the mesh			
	void Normalize(Mesh &values);												//	Normalizes the heightMap

	float lowest = 255;			//	Lowest vertex point
	float highest = 0;			//	highest vertex point

public:
	Mesh * terrain;				//	Mesh to add heightMap to

	int height;					//	Height of image
	int width;					//	Width of image
	int nrChannels;				//	Number of color channels
	float amplitude = 255.0f;	//	Used to adjust height to be within 0 to 1

	void GenerateHeightMap(Mesh &mesh, const char* path);	//	Function to create a heightMap
};

