﻿#pragma once

#include <iostream>
#include <vector>
#include <GL/glew.h>
#include <GLFW/glfw3.h>


//	Initialization functions
bool initWindow();
void initTextures();
void initShaders();
void initMeshes();
void initGameObjects();
void initAudio();

//	Input functions
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods);
void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos);
void windowsize_callback(GLFWwindow* window, int width, int height);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
