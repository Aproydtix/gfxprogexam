#pragma once

#include "Mesh.hpp"


enum FileExt
{
    OBJ,
};

std::vector <Mesh> loadObject(std::string fileName);


