#pragma once

#include "GameObject.h"
#include "Enums.h"

#include <glm/glm/glm.hpp>


//	Camera object, multiple can be created, but the framework only assumes you use mainCamera
class Camera : public GameObject
{
private:
    CameraMode mode;				//	Current camera mode

    float rotationAngle;			//	Rotation angle
    float rotationSpeed;			//	Rotation speed

    glm::vec3 followDistance;		//	Distance from target as a vector
    glm::vec3 upDirection;			//	Up Direction
    glm::vec3 lookatDirection;		//	Look at direction
    glm::vec3 rotationDirection;	//	Rotation direction
    glm::vec3 lookatPosition;		//	Position to look at

    glm::mat4 projection;			//	Camera projection
    glm::mat4 view;					//	Camera view

    GameObject* target;				//	GameObject to look at

public:
    Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode);

	//	Update functions
    void update();
	void updateProjection(int width, int height);
    void input(int key, int scancode, int action, int mods);

	//	Rotation functions
    void rotate(glm::vec3 dir);
    void tilt(glm::vec3 dir);

	//	Set functions
    void setCamMode(CameraMode newMode);
    void setFollowDistance(glm::vec3 newDistance);
    void setUpDirection(glm::vec3 newDirection);
    void setLookatDirection(glm::vec3 newDirection);
    void setLookatPosition(glm::vec3 newPosition);
    void setRotationDirection(glm::vec3 newDirection);
    void setRotationAngle(float angle);
    void setRotationSpeed(float speed);
    void setTarget(GameObject* newTarget);

	//	Get functions
	CameraMode getCamMode();
    GameObject* getTarget();
    glm::vec3 getUpDirection();
    glm::vec3 getLookatDirection();
    glm::vec3 getFollowDistance();
    glm::vec3 getLookatPosition();
    glm::mat4 getProjection();
    glm::mat4 getView();
};