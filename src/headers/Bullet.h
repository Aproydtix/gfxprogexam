#pragma once

#include "GameObject.h"
#include "Enums.h"



class Bullet : public GameObject
{
public:
	bool isActive = false;			//	Whether the Balloon can be collected or not
	glm::vec3 moveDir;				//	Direction vector

	Bullet();
	virtual void update();
	virtual void draw();
};