#include "headers/Init.h"
#include "headers/Camera.h"
#include "headers/Functions.h"
#include "headers/Globals.h"
#include "headers/ObjectHandler.h"
#include "headers/Logger.h"
#include "headers/Audio.hpp"

#include <iostream>
#include <SDL.h>
#undef main

GLFWwindow* g_window;


int main()
{
	//	Initializing
	SDL_Init(SDL_INIT_AUDIO);
    initWindow();
	Start();

	//	deltaTime
    double currentframe = glfwGetTime();
    double lastframe = currentframe;

	//	Game loop
    while (!glfwWindowShouldClose(g_window))
    {
		//	deltaTime
        currentframe = glfwGetTime();
        g_deltatime = currentframe - lastframe;

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//	Update functions
		Update();
		UpdateGameObjects();
		LateUpdate();
        mainCamera->update();
		

		//	Draw all GameObjects
		DrawGameObjects();
        glfwSwapBuffers(g_window);

        glfwPollEvents();

        lastframe = currentframe;
    }

	//	Terminate program
    glfwTerminate();
	return 0;
}
