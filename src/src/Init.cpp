﻿#include "src/headers/Init.h"
#include "src/headers/logger.h"
#include "src/headers/mad.h"
#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/HeightMap.h"
#include "src/headers/Balloon.h"

#include <glm/glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

#define AUDIO_IMPLEMENTATION
#include "src/headers/Audio.hpp"

//	OpenGL variables
extern GLFWwindow* g_window;
extern Camera* mainCamera;
extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;
extern glm::mat4 g_projection;
extern glm::mat4 g_view;

//	Mouse variables
bool mouseLocked = true;
extern int lastX;
extern int lastY;

//	Initializes the window
bool initWindow()
{
    glfwInit();                                                             // Inits glfw
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);                          // Sets major openGL version
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);                          // Sets minor openGL version
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);          // Sets that we want to use core functionality
    //glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	//	Creates window
    g_window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "IMT2531 - Exam", NULL, NULL); 

    if (g_window == NULL)		// If no window created, terminates program
    {
        LOG_ERROR("Error creating window, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Window created");

    glfwMakeContextCurrent(g_window);								//	Sets window to be used
    glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_FALSE);			//	Set input
    glfwSetKeyCallback(g_window, inputHandler);						//	Set keyboard input
	glfwSetCursorPosCallback(g_window, cursor_pos_callback);		//	Set cursor position input
	glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);	//	Set cursor input mode
	glfwSetMouseButtonCallback(g_window, mouse_button_callback);	//	Set mous button input
	
	//	Mouse variables
	lastX = SCREEN_WIDTH / 2.0f;
	lastY = SCREEN_HEIGHT / 2.0f;
	mousePos = glm::vec2(lastX, lastY);

	glewExperimental = GL_TRUE;		// For Linux/Mac OS

    if (glewInit() != GLEW_OK)                                              // Inits glew, terminates if not
    {
        LOG_ERROR("Failed to init glew, terminating...\n");
        glfwTerminate();
        return false;
    }

    LOG_DEBUG("Glew initiated");

    glEnable(GL_BLEND);                                                     // Enables use of blending
    glEnable(GL_DEPTH_TEST);                                                // Depth test so that only textures on top get rendered
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);                      // Enables the use of alpha channel
	/*glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);*/

    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);                          // Sets the viewport coordinates and size
    glfwSetFramebufferSizeCallback(g_window, windowsize_callback);          // Sets what function to call when screen gets resized
    glClearColor(0.1f, 0.4f, 0.9f, 1.0f);                                   // Sets a clear color / black here
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer
    glfwSwapBuffers(g_window);                                              // Swaps buffer
    glClear(GL_COLOR_BUFFER_BIT);                                           // Clears buffer

	TTF_Init();																// Init font


    LOG_DEBUG("Initialization successful");
    return true;
}

//	Loads textures
void initTextures()
{
	whiteTex = LoadTexture("white");
	gliderTex = LoadTexture("glider");
	yellowTex = LoadTexture("yellow");
	heightTex = LoadTexture("height100");
	waterMelonTex = LoadTexture("watermelon");
	greyTex = LoadTexture("grey");
}

//	Loads shaders
void initShaders()
{
	terrainShader = LoadShader("height");
	standardShader = LoadShader("standard");
	textShader = LoadShader("text");
	BaWShader = LoadShader("BaW");
}

//	Loads meshes
void initMeshes()
{
	heightMesh = new Mesh(); //	Used for heightmap
	gliderMesh = LoadObject("glider");
	sunMesh = LoadObject("watermelon");
	bulletMesh = LoadObject("pellet");

	//	Create flat surface
	std::vector<glm::vec3> vert =
	{
		glm::vec3(-0.5f, -0.5f, 0.0f),
		glm::vec3(+0.5f, -0.5f, 0.0f),
		glm::vec3(+0.5f, +0.5f, 0.0f),
		glm::vec3(+0.5f, +0.5f, 0.0f),
		glm::vec3(-0.5f, +0.5f, 0.0f),
		glm::vec3(-0.5f, -0.5f, 0.0f),
	};
	std::vector<glm::vec3> norm =
	{
		glm::vec3(-0.5f, -0.5f,  0.0f),
		glm::vec3(+0.5f, -0.5f,  0.0f),
		glm::vec3(+0.5f, +0.5f,  0.0f),
		glm::vec3(+0.5f, +0.5f,  0.0f),
		glm::vec3(-0.5f, +0.5f,  0.0f),
		glm::vec3(-0.5f, -0.5f,  0.0f),
	};
	std::vector<glm::vec2> textCoord =
	{
		glm::vec2(0.0f, 1.0f),
		glm::vec2(1.0f, 1.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(0.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
	};

	planeMesh = new Mesh();
	planeMesh->vertices = vert;
	planeMesh->normals = norm;
	planeMesh->texCoord = textCoord;
}

//Loads GameObjects
void initGameObjects()
{
	//	Terrain / heightMap
	terrain = new Terrain();
	HeightMap heightMap;
	heightMap.GenerateHeightMap(*heightMesh, "../resources/heightmap/height400.png");
	InitGameObject(terrain, heightMesh, heightTex, terrainShader);
	terrain->transform.scale = glm::vec3(150.0f, 20.0f, 300.0f);
	glm::quat q = glm::quat(glm::vec3(0, PI / 2, 0));
	terrain->transform.rotation *= glm::toMat4(q);
	terrain->material.specularity = 32.0f;

	//	Water
	water = new GameObject();
	InitGameObject(water, planeMesh, whiteTex, standardShader);
	water->transform.scale = glm::vec3(terrain->transform.scale.x * 2.025f, terrain->transform.scale.z * 0.525f, 1);
	water->transform.position = glm::vec3(0, terrain->transform.position.y + 1, 0);
	water->material.specularity = 10.0f;
	q = glm::quat(glm::vec3(PI / 2, 0, 0));
	water->transform.rotation *= glm::toMat4(q);

	//	Glider
	glider = new Glider();
	InitGameObject(glider, gliderMesh, gliderTex, standardShader);
	glider->startPos = glider->transform.position = glm::vec3(0, 30, 0);
	glider->transform.scale = glm::vec3(.5f);

	//	Sun
	sun = new GameObject();
	InitGameObject(sun, sunMesh, yellowTex, textShader);
	sun->transform.scale = glm::vec3(10.0f);

	//	Textboxes
	TTF_Font* font = TTF_OpenFont("../resources/fonts/OpenSans-Regular.ttf", 20);
	seasonText = new TextBox(font, "season", { 255, 255, 255, 255 }, glm::vec3(-0.7, 0.35, 0), left, textShader);
	seasonText->transform.scale = glm::vec3(.06f);
	InitGameObject(seasonText, seasonText->mesh, seasonText->material.texture, seasonText->material.shader);

	dayText = new TextBox(font, "daylight", { 255, 255, 255, 255 }, glm::vec3(0.7, 0.35, 0), right, textShader);
	dayText->transform.scale = glm::vec3(.06f);
	InitGameObject(dayText, dayText->mesh, dayText->material.texture, dayText->material.shader);
	
	speedText = new TextBox(font, "speed", { 255, 255, 255, 255 }, glm::vec3(-0.7, -0.4, 0), left, textShader);
	speedText->transform.scale = glm::vec3(.06f);
	InitGameObject(speedText, speedText->mesh, speedText->material.texture, speedText->material.shader);

	cameraText = new TextBox(font, "cam", { 255, 255, 255, 255 }, glm::vec3(0.7, -0.4, 0), right, textShader);
	cameraText->transform.scale = glm::vec3(.06f);
	InitGameObject(cameraText, cameraText->mesh, cameraText->material.texture, cameraText->material.shader);

	scoreText = new TextBox(font, "score", { 255, 255, 255, 255 }, glm::vec3(0, 0.35, 0), center, textShader);
	scoreText->transform.scale = glm::vec3(.06f);
	InitGameObject(scoreText, scoreText->mesh, scoreText->material.texture, scoreText->material.shader);

	//	Balloons
	for (int i = 0; i < 10; i++)
	{
		Balloon* balloon = new Balloon();
		InitGameObject(balloon, sunMesh, waterMelonTex, standardShader);
		balloon->transform.position = glm::vec3(rand() % 300 - 150, 10 + rand() % 40, rand() % 150 - 75);
		balloons.push_back(balloon);
	}

	//	Bullets
	for (int i = 0; i < 20; i++)
	{
		Bullet* bullet = new Bullet();
		InitGameObject(bullet, bulletMesh, greyTex, standardShader);
		bullets.push_back(bullet);
	}
}

//	Loads audio files and plays music when game starts
void initAudio()
{
	Audio::InitAudio();
	Audio::AddBGM("../resources/sound/Elysium, in the Blue Sky.wav", "Elysium", false);
	Audio::AddEffect("../resources/sound/sfx1.wav", "PickUp");
	Audio::AddEffect("../resources/sound/sfx2.wav", "Shoot");
	Audio::PlayBGM("Elysium", -1);
}

//	Callback function for when the window resizes
void windowsize_callback(GLFWwindow* window, int width, int height)
{

    SCREEN_WIDTH = width;
    SCREEN_HEIGHT = height;

    mainCamera->updateProjection(width, height);
    glfwGetWindowSize(window, &height, &width);
    glViewport(0, 0, height, width);
}

//	Callback function for mouse position
void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos)
{
	mousePos = glm::vec2(xpos, ypos);
}

//	Callback function for mouse button input
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		glider->shoot();
	}
}

//	Callback function for keyboard input
void inputHandler (GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		mouseLocked = !mouseLocked;
		if (mouseLocked)
			glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		else
			glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}

    HandleInput(g_window, key, scancode, action, mods);
}

