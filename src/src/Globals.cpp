#include "src/headers/Globals.h"

//	Global data
int SCREEN_WIDTH = 1280;
int SCREEN_HEIGHT = 720;

double g_deltatime;
Camera* mainCamera;
glm::vec2 mousePos;
int lastX;
int lastY;

//	Lists of loaded things
std::vector<Light*> lights;
std::vector<GameObject*> gameObjects;
std::vector<Mesh*> meshes;
std::vector<Texture*> textures;
std::vector<Shader*> shaders;

//----------------------------------------------------------------------
//	Project specific variables
//----------------------------------------------------------------------

//	Tetxures
Texture* whiteTex;
Texture* gliderTex;
Texture* yellowTex;
Texture* heightTex;
Texture* waterMelonTex;
Texture* greyTex;

//	Shaders
Shader* terrainShader;
Shader* standardShader;
Shader* textShader;
Shader* BaWShader;

//	Meshes
Mesh* heightMesh;
Mesh* gliderMesh;
Mesh* sunMesh;
Mesh* planeMesh;
Mesh* bulletMesh;

//	Textboxes
TextBox* seasonText;
TextBox* dayText;
TextBox* speedText;
TextBox* cameraText;
TextBox* scoreText;

//	Terrain
Terrain* terrain;
bool hardLines = false;
float season = 1;
float min;
float max;
glm::vec4 waterColor;
glm::vec4 vegetationColor;
glm::vec4 mountainColor;
glm::vec4 snowColor;
float dayTime = 6;

//	Other GameObjects
Glider* glider;
GameObject* water;
GameObject* sun;

//	Game variables
int score = 0;
std::vector<Balloon*> balloons;
std::vector<Bullet*> bullets;