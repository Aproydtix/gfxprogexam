#include "src/headers/HeightMap.h"
#include "src/headers/stb_image.h"


//	Calculates the normal of a face
glm::vec3 HeightMap::FaceNormal(Triangle tri)	
{
	return glm::cross(tri.B - tri.A, tri.C - tri.A);
}

//	Calculates the normal of a vertex
glm::vec3 HeightMap::VertexNormal(GLfloat* values, glm::vec2 &size, glm::vec2 &pos)	
{
	//	The normal of the vector is the sum of the normals of adjacent faces
	glm::vec3 sum(0.0f);	
	int w = size.x, h = size.y;

	//	All adjacent faces
	Triangle tris[6];	
	values[int(pos.x * w + pos.y)];

	//	Middle
	for (Triangle t : tris) t.A = t.B = t.C = glm::vec3(
		(pos.x - float(h) / 2) / float(h),
		values[int(pos.x * w + pos.y)],
		(pos.y - float(w) / 2) / float(w));
	//	Top Left
	if (pos.x != 0 && pos.y != 0) tris[0].C = tris[1].B = glm::vec3(
		(pos.x - 1 - float(h) / 2) / float(h), 
		values[int((pos.x - 1)* w + (pos.y - 1))], 
		(pos.y - 1 - float(w) / 2) / float(w));
	//	Left
	if (pos.x != 0) tris[1].C = tris[2].B = glm::vec3(
		(pos.x - 1 - float(h) / 2) / float(h),
		values[int((pos.x - 1) * w + pos.y)], 
		(pos.y - float(w) / 2) / float(w));
	//	Bottom
	if (pos.y != w - 1) tris[2].C = tris[3].B = glm::vec3(
		(pos.x - float(h) / 2) / float(h), 
		values[int(pos.x * w + (pos.y + 1))], 
		(pos.y + 1 - float(w) / 2) / float(w));
	//	Bottom Right 
	if (pos.x != h - 1 && pos.y != w - 1) tris[3].C = tris[4].B = glm::vec3(
		(pos.x + 1 - float(h) / 2) / float(h), 
		values[int((pos.x + 1) * w + (pos.y + 1))], 
		(pos.y + 1 - float(w) / 2) / float(w));
	//	Right
	if (pos.x != h - 1) tris[4].C = tris[5].B = glm::vec3(
		(pos.x + 1 - float(h) / 2) / float(h), 
		values[int((pos.x + 1) * w + pos.y)], 
		(pos.y - float(w) / 2) / float(w));
	//	Top
	if (pos.y != 0) tris[5].C = tris[0].B = glm::vec3(
		(pos.x - float(h) / 2) / float(h), 
		values[int(pos.x * w + (pos.y - 1))], 
		(pos.y - 1 - float(w) / 2) / float(w));

	//	Sum of the face normals
	for (Triangle t : tris) 
		sum += FaceNormal(t);

	return sum;
}

//	Creates the mesh of the heightMap
Mesh HeightMap::CreateMesh(GLfloat* heightValues, glm::vec2 size)
{
	Mesh mesh;

	for (int i = 1; i < size.y; i++)
	{
		for (int j = 1; j < size.x; j++)
		{
			//	Find smallest and highest value
			if (i < size.y - 1 && j < size.x - 1)
			{
				if (heightValues[int(i * size.x + j)] < lowest) lowest = heightValues[int(i * size.x + j)];
				if (heightValues[int(i * size.x + j)] > highest) highest = heightValues[int(i * size.x + j)];
			}

			//	Vertices
			glm::vec3 brVector(
				(i - float(size.y) / 2) / float(size.y), 
				heightValues[int(i * size.x + j)], 
				(j - float(size.x) / 2) / float(size.x));
			glm::vec3 blVector(
				((i - 1) - float(size.y) / 2) / float(size.y),
				heightValues[int((i - 1) * size.x + j)], 
				(j - float(size.x) / 2) / float(size.x));
			glm::vec3 trVector(
				(i - float(size.y) / 2) / float(size.y), 
				heightValues[int(i * size.x + (j - 1))],
				((j - 1) - float(size.x) / 2) / float(size.x));
			glm::vec3 tlVector(
				((i - 1) - float(size.y) / 2) / float(size.y),
				heightValues[int((i - 1) * size.x + (j - 1))],
				((j - 1) - float(size.x) / 2) / float(size.x));

			mesh.vertices.push_back(brVector);	//	Bottom Right
			mesh.vertices.push_back(tlVector);	//	Top Left
			mesh.vertices.push_back(trVector);	//	Top Right
			mesh.vertices.push_back(brVector);	//	Bottom Right
			mesh.vertices.push_back(tlVector);	//	Top Left
			mesh.vertices.push_back(blVector);	//	Bottom Left


			//	Texture coordinates
			glm::vec2 brTex(i / size.y, j / size.x);
			glm::vec2 blTex((i - 1) / size.y, j / size.x);
			glm::vec2 trTex(i / size.y, (j - 1) / size.x);
			glm::vec2 tlTex((i - 1) / size.y, (j - 1) / size.x);

			mesh.texCoord.push_back(brTex);	//	Bottom Right
			mesh.texCoord.push_back(tlTex);	//	Top Left
			mesh.texCoord.push_back(trTex);	//	Top Right
			mesh.texCoord.push_back(brTex);	//	Bottom Right
			mesh.texCoord.push_back(tlTex);	//	Top Left
			mesh.texCoord.push_back(blTex);	//	Bottom Left


			//	Normals
			glm::vec3 brNorm(VertexNormal(heightValues, size, glm::vec2(i, j)));
			glm::vec3 blNorm(VertexNormal(heightValues, size, glm::vec2(i - 1, j)));
			glm::vec3 trNorm(VertexNormal(heightValues, size, glm::vec2(i, j - 1)));
			glm::vec3 tlNorm(VertexNormal(heightValues, size, glm::vec2(i - 1, j - 1)));

			mesh.normals.push_back(brNorm);	//	Bottom Right
			mesh.normals.push_back(tlNorm);	//	Top Left
			mesh.normals.push_back(trNorm);	//	Top Right
			mesh.normals.push_back(brNorm);	//	Bottom Right
			mesh.normals.push_back(tlNorm);	//	Top Left
			mesh.normals.push_back(blNorm);	//	Bottom Left
		}
		//std::cout << "Loaded row: " << i << "\n";
	}
	return mesh;
}

//	Normalizes the heightMap scale
void HeightMap::Normalize(Mesh &values)
{
	for (glm::vec3 &v : values.vertices)
		v.y = (v.y - lowest) / (highest - lowest);
	for (glm::vec3 &n : values.normals)
		n.y = n.y * (highest - lowest);
}

//	Generates a heightMap and applies it to the mesh
void HeightMap::GenerateHeightMap(Mesh &mesh, const char* path)
{
	//	Read texture data
	unsigned char* data = stbi_load(path, &width, &height, &nrChannels, 0);
	std::string heightString(reinterpret_cast<char*>(data));
	stbi_image_free(data);

	//	All the height data
	GLfloat* terrainHeight = new GLfloat[width * height];	

	for (int i = 0; i < height; i++)
		for (int j = 0; j < width; j++)
		{
			float altitude = 0;
			if (i != 0 && j != 0 && i != height - 1 && j != width - 1)
			{
				for (int k = 0; k < nrChannels; k++) 
					altitude += heightString[i * nrChannels * width + j * nrChannels + k];

				altitude = altitude / nrChannels;
			}
			terrainHeight[int(i * width + j)] = altitude;
		}

	//	Create mesh object
	terrain = new Mesh(CreateMesh(terrainHeight, glm::vec2(width, height)));
	Normalize(*terrain);
	mesh = *terrain;
}

