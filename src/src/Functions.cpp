#include "src/headers/Functions.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Init.h"
#include "src/headers/Globals.h"
#include "src/headers/Camera.h"
#include "src/headers/Terrain.h"
#include "src/headers/TextBox.h"

#include <glm/glm/gtx/quaternion.hpp>
#include <iomanip>
#include <sstream>

//--------------------------------------------------------------------
//	USED TO INITIALIZE THE GAME AND YOUR GAMEOBJECTS, CLASSES, AND FUNCTIONS
//--------------------------------------------------------------------
void Start()
{
	//	Load files and create objects
	initTextures();
	initShaders();
	initMeshes();
	initGameObjects();
	initAudio();

	//	Create Light and Camera
	mainCamera = new Camera(glm::vec3(0, 0, 5), glm::vec3(0, 0, -1), glm::vec3(0, 1, 0), trackingCam);
	Light* light = CreateLight(glm::vec3(1.0f));

	//	Set initial season values and colors
	float s = ((season - 1.0f) / 4.0f)* PI * 2 - PI / 2;
	min = (0.06f + ((glm::sin(s)+1)/2) * 0.02f) * terrain->transform.scale.y;
	max = (0.225f + glm::cos(s) * 0.04f) * terrain->transform.scale.y;
	waterColor = glm::vec4(.25f, .5f, 1.0f, 1.0f);
	vegetationColor = glm::vec4(.3f - glm::cos(s) / 8 + glm::sin(s) / 2, .75f + glm::cos(s) / 8, .2f - glm::sin(s) / 16, 1.0f);
	mountainColor = glm::vec4(.45f, .45f, .25f, 1.0f);
	snowColor = glm::vec4(.8f, .9f, 1.0f, 1.0f);

	//	Initialize Camera
	InitGameObject(mainCamera, nullptr, nullptr, nullptr);
	mainCamera->setTarget(terrain);
	mainCamera->transform.position = glm::vec3(0, 50, -5);
	mainCamera->setLookatPosition(glm::vec3(0, -10, 0));
	mainCamera->setFollowDistance(glm::vec3(0, 60, 75));
	mainCamera->setTarget(glider);
}

//--------------------------------------------------------------------
//	USED TO UPDATE THE GAME, OTHERWISE THE SAME AS START
//--------------------------------------------------------------------
void Update()
{
	
}

//	Late Update, called after all other updates
void LateUpdate()
{
	//	Update season variables
	float s = ((season - 1.0f) / 4.0f)* PI * 2 - PI / 2;
	min = (0.01f + ((glm::sin((s + PI/2)*2) + 1) / 2) * 0.05f) * terrain->transform.scale.y;
	max = (0.65f + glm::cos(s) * 0.25f) * terrain->transform.scale.y;
	waterColor = glm::vec4(.25f, .5f, 1.0f, 1.0f);
	vegetationColor = glm::vec4(.3f - glm::cos(s) / 8 + glm::sin(s) / 2, .75f + glm::cos(s) / 8, .2f - glm::sin(s) / 16, 1.0f);
	water->transform.position = terrain->transform.position + min;
	water->material.color = waterColor;

	//	Set season text
	std::string sStr = "";
	if (glm::floor(season + .5f) == 1)
		sStr = "Spring";
	if (glm::floor(season + .5f) == 2)
		sStr = "Summer";
	if (glm::floor(season + .5f) == 3)
		sStr = "Autumn";
	if (glm::floor(season + .5f) == 4)
		sStr = "Winter";
	if (glm::floor(season + .5f) == 5)
		sStr = "Spring";
	seasonText->text = sStr;

	//	Set day text
	std::string dStr = "";
	if (glm::floor(dayTime + .5f) == 6)
		dStr = "Morning";
	if (glm::floor(dayTime + .5f) == 7)
		dStr = "Noon";
	if (glm::floor(dayTime + .5f) == 8)
		dStr = "Evening";
	if (glm::floor(dayTime + .5f) == 9)
		dStr = "Night";
	if (glm::floor(dayTime + .5f) == 10)
		dStr = "Morning";
	dayText->text = dStr;

	//	Set speed text
	std::stringstream stream;
	if (glider->physics)
		stream << std::fixed << std::setprecision(2) << glm::length(glider->speed);
	else
		stream << std::fixed << std::setprecision(2) << glm::length(glider->engineSpeed);
	std::string spd = stream.str();
	speedText->text = "spd: " + spd;

	//	Set Camera Mode text
	if (mainCamera->getCamMode() == freeCam)
		cameraText->text = "Free";
	if (mainCamera->getCamMode() == staticCam)
		cameraText->text = "Map";
	if (mainCamera->getCamMode() == orbitPositionCam)
		cameraText->text = "Orbit Map";
	if (mainCamera->getCamMode() == trackingCam)
		cameraText->text = "Tracking";
	if (mainCamera->getCamMode() == followCam)
		cameraText->text = "3rd Person";
	if (mainCamera->getCamMode() == firstPersonCam)
		cameraText->text = "1st Person";
	if (mainCamera->getCamMode() == orbitTargetCam)
		cameraText->text = "Orbit Glider";

	//	Set score text
	scoreText->text = "Score: " + std::to_string(score);
}