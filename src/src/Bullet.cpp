#include "src/headers/Bullet.h"
#include "src/headers/Globals.h"
#include "src/headers/Audio.hpp"

Bullet::Bullet()
{

}

void Bullet::update()
{
	if (isActive)
	{
		for (int j = 0; j < 10; j++)
		{
			//	Move
			transform.position += moveDir/10.0f;

			//	Collect
			for (int i = 0; i < balloons.size(); i++)
				if (glm::length(balloons[i]->transform.position - transform.position) < 2)
				{
					score += 10;
					balloons[i]->transform.position = glm::vec3(rand() % 300 - 150, 10 + rand() % 40, rand() % 150 - 75);
					Audio::PlayEffect("PickUp");
				}
		}
	}
}

void Bullet::draw()
{
	if (isActive)
	{
		//	Check whether GameObject has required components
		if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
		{
			//	Standard Shader
			material.shader->setMatrix("view", mainCamera->getView());
			material.shader->setVec3("lightPos", lights[0]->transform.position);	//	GET FROM GLOBAL LIGHT
			material.shader->setVec3("lightColor", lights[0]->color);				//	GET FROM GLOBAL LIGHT
			material.shader->setVec3("camPos", mainCamera->transform.position);		//	GET FROM GLOBAL CAMERA
			material.shader->setFloat("specularity", material.specularity);

			drawInternal(mainCamera->getView());
		}
	}
}