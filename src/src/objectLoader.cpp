#include "src/headers/ObjectLoader.h"
#include "src/headers/Logger.h"

#include <fstream>
#include <string>


//	Loads .obj into a Mesh object
std::vector <Mesh> loadObject(std::string fileName)
{
    std::vector <Mesh> obj;
    std::ifstream in;
    char mtllib[100];
    FileExt fileExt;

    if (fileName.substr(fileName.find_last_of(".") + 1) == "obj")
        fileExt = OBJ;

    switch (fileExt)
    {
    case OBJ:
        in.open(fileName);
        if (!in.is_open())
        {
            //std::cout << "Failed to open object file\n\n";
            LOG_DEBUG(("Failed to open object: " + fileName).c_str());
        }
        else
        {
            std::vector<glm::vec3> vertices;
            std::vector<glm::vec2> texCoord;
            std::vector<glm::vec3> normals;
            char dummy[100];
            in.ignore(100, '\n');
            in.ignore(100, '\n');
            in.ignore(10, ' ');
            in.getline(mtllib, 100);

            while (!in.eof())
            {
                in.getline(dummy, 100, ' ');
                if (dummy[0] == 'o' || dummy[0] == 'g')
                {
                    obj.resize(obj.size() + 1);
                    obj[obj.size() - 1].mtllib = mtllib;
                }
                else if (dummy[0] == 'v' && dummy[1] == 'n')
                {
                    GLfloat x, y, z;
                    in >> x;
                    in >> y;
                    in >> z;
                    normals.push_back(glm::vec3(x, y, z));
                }
                else if (dummy[0] == 'v' && dummy[1] == 't')
                {
                    GLfloat x, y;
                    in >> x;
                    in >> y;
                    texCoord.push_back(glm::vec2(x, y));
                }
                else if (dummy[0] == 'v')
                {
                    GLfloat x, y, z;
                    in >> x;
                    in >> y;
                    in >> z;
                    vertices.push_back(glm::vec3(x, y, z));
                }
                else if (dummy[0] == 'f')
                {
                    std::vector<unsigned int> v, n, t;
                    v.resize(3);
                    n.resize(3);
                    t.resize(3);

                    for (int i = 0; i < 3; i++)
                    {
                        in >> v[i];
                        in.ignore(10, '/');
                        if(texCoord.size() != 0)
                            in >> t[i];
                        in.ignore(10, '/');
                        in >> n[i];
                        v[i]--;
                        t[i]--;
                        n[i]--;
                    }

                    obj[obj.size() - 1].vertices.push_back(vertices[v[0]]);
                    obj[obj.size() - 1].vertices.push_back(vertices[v[1]]);
                    obj[obj.size() - 1].vertices.push_back(vertices[v[2]]);

                    if (texCoord.size() != 0)
                    {
                        obj[obj.size() - 1].texCoord.push_back(texCoord[t[0]]);
                        obj[obj.size() - 1].texCoord.push_back(texCoord[t[1]]);
                        obj[obj.size() - 1].texCoord.push_back(texCoord[t[2]]);
                    }

                    obj[obj.size() - 1].normals.push_back(normals[n[0]]);
                    obj[obj.size() - 1].normals.push_back(normals[n[1]]);
                    obj[obj.size() - 1].normals.push_back(normals[n[2]]);
                }
                in.ignore(100, '\n');
            }
        }

        break;
    default:
        LOG_DEBUG("File format not supported. Empty Object returned");
        break;
    }
    return obj;
}
