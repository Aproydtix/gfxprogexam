#include "src/headers/Terrain.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"


Terrain::Terrain()
{
    //  Default constructor
}

//	Draws the terrain with the special shader, coloring heights
void Terrain::draw()
{
	if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
	{
		//	Standard Shader
		material.shader->setMatrix("view", mainCamera->getView());
		material.shader->setVec3("lightPos", lights[0]->transform.position);	//	GET FROM GLOBAL LIGHT
		material.shader->setVec3("lightColor", lights[0]->color);				//	GET FROM GLOBAL LIGHT
		material.shader->setVec3("camPos", mainCamera->transform.position);		//	GET FROM GLOBAL CAMERA
		material.shader->setFloat("specularity", material.specularity);

		//	Terrain Shader
		material.shader->setInt("hardLines", hardLines);
		material.shader->setFloat("minH", terrain->transform.position.y + min);
		material.shader->setFloat("maxH", terrain->transform.position.y + max);
		material.shader->setVec4("waterColor", waterColor);
		material.shader->setVec4("vegetationColor", vegetationColor);
		material.shader->setVec4("mountainColor", mountainColor);
		material.shader->setVec4("snowColor", snowColor);

		drawInternal(mainCamera->getView());
	}
}

//	Updates terrain values
void Terrain::update()
{
	//	Updates seasons
	if (changingSeasons)
		season += (float)g_deltatime / 16.0f;
	else
		season = season + (seasonGoal - season) * g_deltatime*2;

	if (season > 5)
		season = 1;

	//	Update day, sun, and lighting
	float day = ((dayTime-6)/4) * PI*2;
	float s = ((season - 1.0f) / 4.0f)* PI * 2 + PI/2;
	float d = 400.0f;

	//	Sun position
	sun->transform.position = lights[0]->transform.position = glm::vec3(
		glm::sin(day)*d * (.25f+(glm::sin(s)+1)/4), 
		glm::sin(day)*d * (.25f+(glm::cos(s+PI)+1)/4), 
		-glm::cos(day)*d);

	//	Sky coloring
	glm::vec3 c = glm::vec3(
		0.1f*(glm::sin(day)+0.25f) - glm::cos(day)/6, 
		0.4f*(glm::sin(day)+0.25f), 
		0.9f*(glm::sin(day)+0.25f)) 
		
		* (float)(.25f + (((glm::cos(s+PI)+1)/4)*.75f));

	glClearColor(c.x, c.y, c.z, 1.0f);

	//	Light
	lights[0]->color = glm::vec3(glm::clamp(glm::sin(day) + 0.25f * (float)(glm::cos(s+PI)), 0.0f, 1.0f));
	
	//	Update dayTime value
	if (changingDayTime)
		dayTime += (float)g_deltatime / 4.0f;
	else
		dayTime = dayTime + (dayGoal - dayTime) * g_deltatime*8;

	if (dayTime > 10)
		dayTime = 6;
}

//	Input to change seaons and time
void Terrain::input(int key, int scancode, int action, int mods)
{
	//	Hard / Soft lines
	if (key == GLFW_KEY_O && action == GLFW_RELEASE)
		hardLines = !hardLines;

	//	Seasons
	if (key == GLFW_KEY_1 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 1;
	}

	if (key == GLFW_KEY_2 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 2;
	}

	if (key == GLFW_KEY_3 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 3;
	}

	if (key == GLFW_KEY_4 && action == GLFW_RELEASE)
	{
		changingSeasons = false;
		seasonGoal = 4;
	}
	//	Stop seasons
	if (key == GLFW_KEY_5 && action == GLFW_RELEASE)
	{
		changingSeasons = !changingSeasons;
		seasonGoal = season;
	}


	//	Daytime
	if (key == GLFW_KEY_6 && action == GLFW_RELEASE)
	{
		changingDayTime = false;
		dayGoal = 6;
	}
	
	if (key == GLFW_KEY_7 && action == GLFW_RELEASE)
	{
		changingDayTime = false;
		dayGoal = 7;
	}

	if (key == GLFW_KEY_8 && action == GLFW_RELEASE)
	{
		changingDayTime = false;
		dayGoal = 8;
	}

	if (key == GLFW_KEY_9 && action == GLFW_RELEASE)
	{
		changingDayTime = false;
		dayGoal = 9;
	}
	//	Stop seasons
	if (key == GLFW_KEY_0 && action == GLFW_RELEASE)
	{
		changingDayTime = !changingDayTime;
		dayGoal = dayTime;
	}
}
