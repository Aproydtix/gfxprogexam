#include "src/headers/Balloon.h"
#include "src/headers/Globals.h"
#include "src/headers/Audio.hpp"

Balloon::Balloon()
{

}

void Balloon::update()
{
	if (isActive)
	{
		//	Rotate and bob
		transform.position += glm::vec3(0, glm::cos(glfwGetTime()*5.0f)/20.0f, 0);
		transform.rotate(45, glm::vec3(0, 1, 0), g_deltatime*2);

		//	Collect
		if (glm::length(glider->transform.position - transform.position) < 2)
		{
			score += 10;
			transform.position = glm::vec3(rand() % 300 - 150, 10 + rand() % 40, rand() % 150 - 75);
			Audio::PlayEffect("PickUp");
		}
	}
}

void Balloon::draw()
{
	if (isActive)
	{
		//	Check whether GameObject has required components
		if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
		{
			//	Standard Shader
			material.shader->setMatrix("view", mainCamera->getView());
			material.shader->setVec3("lightPos", lights[0]->transform.position);	//	GET FROM GLOBAL LIGHT
			material.shader->setVec3("lightColor", lights[0]->color);				//	GET FROM GLOBAL LIGHT
			material.shader->setVec3("camPos", mainCamera->transform.position);		//	GET FROM GLOBAL CAMERA
			material.shader->setFloat("specularity", material.specularity);

			drawInternal(mainCamera->getView());
		}
	}
}