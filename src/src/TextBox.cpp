#include "src/headers/TextBox.h"
#include "src/headers/Globals.h"


//	Create textbox
TextBox::TextBox(TTF_Font* fnt, std::string txt, SDL_Color clr, glm::vec3 pos, Allignment allign, Shader* shader)
{
	//	Set values
	text = txt;
	color = clr;
	allignment = allign;
	font = fnt;
	transform.position = pos;

	//	Set pointers
	material.texture = new Texture();
	material.shader = shader;
	buffer = new Buffer;
	mesh = new Mesh();

	//	Create text
	createText();
}

//	Update text
void TextBox::update()
{
	if (text != prevText)
	{
		bool meshUpdate = false;
		if (text.length() != prevText.length())
			meshUpdate = true;
		
		createText();
		
		if (meshUpdate)
			setMeshBuffers();
	}
}

//	Draw text
void TextBox::draw()
{
	if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
	{
		glm::mat4 view;
		if (isUI)
			view = glm::mat4(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -1.0f)));
		else
			view = mainCamera->getView();

		drawInternal(view);
	}
}

//	Create text
void TextBox::createText()
{
	// Width used for the width of the drawn rectangle
	int width = material.texture->createTextureFromText(font, text, color); // Creates texture from input string

	// Mesh creation
	std::vector<glm::vec3> vertices;

	// Vertices based on allignment
	if (allignment == center)
	{
		vertices = {
			glm::vec3((width / 20) / 2,  1.0f,  0.0f),  // Top Right
			glm::vec3((width / 20) / 2,  0.0f,  0.0f),  // Bottom Right
			glm::vec3((-width / 20) / 2, 0.0f,  0.0f),  // Bottom Left
			glm::vec3((-width / 20) / 2, 1.0f,  0.0f),  // Top Left
			glm::vec3((width / 20) / 2,  1.0f,  0.0f),  // Top Right
			glm::vec3((-width / 20) / 2, 0.0f,  0.0f),  // Bottom Left
		};
	}
	if (allignment == left)
	{
		vertices = {
			glm::vec3((width / 20),  1.0f,  0.0f),      // Top Right
			glm::vec3((width / 20),  0.0f,  0.0f),      // Bottom Right
			glm::vec3(0.0f, 0.0f,  0.0f),               // Bottom Left
			glm::vec3(0.0f, 1.0f,  0.0f),               // Top Left
			glm::vec3((width / 20),  1.0f,  0.0f),      // Top Right
			glm::vec3(0.0f, 0.0f,  0.0f),               // Bottom Left
		};
	}

	if (allignment == right)
	{
		vertices = {
			glm::vec3(0.0f,  1.0f,  0.0f),              // Top Right
			glm::vec3(0.0f,  0.0f,  0.0f),              // Bottom Right
			glm::vec3((-width / 20), 0.0f,  0.0f),      // Bottom Left
			glm::vec3((-width / 20), 1.0f,  0.0f),      // Top Left
			glm::vec3(0.0f,  1.0f,  0.0f),              // Top Right
			glm::vec3((-width / 20), 0.0f,  0.0f),      // Bottom Left
		};
	}

	// Texture coordinates
	std::vector<glm::vec2> texCoords = {

		glm::vec2(1.0f, 0.0f),
		glm::vec2(1.0f, 1.0f),
		glm::vec2(0.0f, 1.0f),
		glm::vec2(0.0f, 0.0f),
		glm::vec2(1.0f, 0.0f),
		glm::vec2(0.0f, 1.0f),
	};

	mesh->vertices = vertices;
	mesh->texCoord = texCoords;
}