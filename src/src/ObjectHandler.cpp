#include "src/headers/ObjectHandler.h"
#include "src/headers/Mad.h"
#include "src/headers/Globals.h"


//	Creates a light
Light* CreateLight(glm::vec3 pos, glm::vec3 color)
{
	Light* light = new Light();
	light->transform.position = pos;
	light->color = color;
	lights.push_back(light);

	return light;
}

//	Initializes a GameObject and adds it to the global list
void InitGameObject(GameObject* go, Mesh* obj, Texture* tex, Shader* shade)
{
	go->init(obj, tex, shade);
}

//	Loads a mesh from .obj file
Mesh* LoadObject(std::string fileName)
{
	std::string path = "../resources/models/";

	Mesh* object = new Mesh(loadObject(path + fileName + ".obj")[0]);
	meshes.push_back(object);

	return object;
}

//	Loads texture
Texture* LoadTexture(std::string fileName)
{
	std::string path = "../resources/textures/";

	Texture* texture = new Texture((path + fileName + ".png").c_str());
	textures.push_back(texture);

	return texture;
}

//	Loads shader
Shader* LoadShader(std::string fileName)
{
	Shader* shader = new Shader(fileName);
	shaders.push_back(shader);

	return shader;
}

//	Updates all GameObjects in the list every frame
void UpdateGameObjects()
{
	for (int i=0; i<gameObjects.size(); i++)
		gameObjects[i]->update();
}

//	Draws all GameObjects in the list every frame
void DrawGameObjects()
{
	for (int i = 0; i<gameObjects.size(); i++)
		gameObjects[i]->draw();
}

//	Handles input from all GameObjects in the list every frame
void HandleInput(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	for (int i = 0; i<gameObjects.size(); i++)
		gameObjects[i]->input(key, scancode, action, mods);
}
