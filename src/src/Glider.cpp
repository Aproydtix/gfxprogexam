#include "src/headers/Glider.h"
#include "src/headers/Globals.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Audio.hpp"

#include <glm/glm/gtx/quaternion.hpp>


//	Initialize Glider
Glider::Glider()
{
	engineSpeed = maxSpeed;
	speed = transform.forward() * maxSpeed / 2.0f;
}

//	Update Glider every frame
void Glider::update()
{
	//	Freezes Glider in place if Camera is in free mode
	if (mainCamera->getCamMode() != freeCam)
	{
		//	Clamp engine speed
		engineSpeed = glm::clamp(engineSpeed, minSpeed, maxSpeed);

		//	Physics-like movement
		if (physics)
		{
			//	Forward propulsion from engine
			speed = speed + (transform.forward() * engineSpeed - speed) * (float)g_deltatime*engineSpeed;
			//	Cap speed
			if (glm::length(speed) > maxSpeed)
				speed = glm::normalize(speed) * maxSpeed;
			//	Lift
			speed += (transform.up() * glm::length(speed) * (float)g_deltatime) * 1.5f;
			//	Gravity
			speed -= glm::vec3(0, 10, 0) * (float)g_deltatime;

			//	Update position
			transform.position += speed * (float)g_deltatime;
		}
		else	//	"Arcade" movement
			transform.position += transform.forward() * engineSpeed * (float)g_deltatime;

		if (glm::length(transform.position) > 200)
			reset();
	}
}

void Glider::input(int key, int scancode, int action, int mods)
{
	//	Freezes Glider in place if Camera is in free mode
	if (mainCamera->getCamMode() != freeCam)
	{
		//	Enable/Disable physics
		if (key == GLFW_KEY_P && action == GLFW_PRESS)
			physics = !physics;

		//	Reset position
		if (key == GLFW_KEY_R && action == GLFW_PRESS)
		{
			reset();
		}

		//	Random position
		if (key == GLFW_KEY_F && action == GLFW_PRESS)
		{
			reset();
			int d = 100;
			transform.position = glm::vec3(startPos.x + rand() % d - d / 2, startPos.y, startPos.z + rand() % d - d / 2);
		}

		//	Roll
		if (key == GLFW_KEY_A)
			transform.rotate(90.0f, glm::vec3(0, 0, -1), g_deltatime * 5);
		if (key == GLFW_KEY_D)
			transform.rotate(90.0f, glm::vec3(0, 0, 1), g_deltatime * 5);

		//	Pitch
		if (key == GLFW_KEY_W)
			transform.rotate(90.0f, glm::vec3(1, 0, 0), g_deltatime * 5);
		if (key == GLFW_KEY_S)
			transform.rotate(90.0f, glm::vec3(-1, 0, 0), g_deltatime * 5);

		//	Yaw
		if (key == GLFW_KEY_Q)
			transform.rotate(90.0f, glm::vec3(0, 1, 0), g_deltatime * 5);
		if (key == GLFW_KEY_E)
			transform.rotate(90.0f, glm::vec3(0, -1, 0), g_deltatime * 5);

		//	Speed
		if (key == GLFW_KEY_COMMA)
			engineSpeed += (10.0f * (float)g_deltatime);
		if (key == GLFW_KEY_PERIOD)
			engineSpeed -= (10.0f * (float)g_deltatime);

		//	Shoot
		if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
		{
			shoot();
		}
	}
}

void Glider::reset()
{
	transform.position = startPos;
	engineSpeed = maxSpeed;
	glm::quat q = glm::quat(glm::vec3(0, (rand() % 360 * PI / 180), 0));
	transform.rotation = glm::mat4(1.0f) * glm::toMat4(q);
	speed = transform.forward() * maxSpeed / 2.0f;
}

void Glider::shoot()
{
	Bullet* bullet = bullets[bullets.size() - 1];
	bullets.insert(bullets.begin(), bullet);
	bullets.resize(bullets.size() - 1);
	bullet->isActive = true;
	bullet->transform.position = transform.position;
	bullet->moveDir = transform.forward() * maxSpeed * 2.0f;
	Audio::PlayEffect("Shoot");
}
