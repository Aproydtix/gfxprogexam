#include "src/headers/Camera.h"
#include "src/headers/Globals.h"
#include "src/headers/logger.h"

#include <glm/glm/gtx/quaternion.hpp>

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;

//	Initialize the camera
Camera::Camera(glm::vec3 pos, glm::vec3 viewDir, glm::vec3 upDir, CameraMode newMode)
{
    transform.position = pos;
    lookatDirection = viewDir;
    upDirection = upDir;
    mode = newMode;

    rotationAngle = 45.0f;
    rotationSpeed = 1.0f;
    rotationDirection = glm::vec3(0, 1, 0);
    lookatPosition = glm::vec3(0, 0, 0);
    followDistance = glm::vec3(0, 0, 1);
    
    view = glm::mat4(1.0f);
    projection = glm::perspective(glm::radians(45.0f), float(SCREEN_WIDTH) / float(SCREEN_HEIGHT), 0.1f, 300.0f);
}

//	Update every frame
void Camera::update()
{
	//	Different Camera Modes
	switch (mode)
	{
	case freeCam:
	{
		float x = (mousePos.x - lastX) / 100.0f;
		float y = (mousePos.y - lastY) / 100.0f;

		transform.yaw(-x);
		transform.pitch(y);

		lastX = mousePos.x;
		lastY = mousePos.y;

		glm::mat4 tarView = glm::lookAt(transform.position, transform.position + transform.forward(), upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 16);
		break;
	}
	case staticCam:
	{
		glm::mat4 tarView = glm::lookAt(transform.position, lookatPosition, upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 8);
		break;
	}
	case followCam:
	{
		glm::vec3 tarPos = target->transform.position - target->transform.forward()*15.0f + glm::vec3(0, 6.0f, 0);
		transform.position = transform.position + (tarPos - transform.position) * (float)(g_deltatime * 8);

		glm::mat4 tarView = glm::lookAt(transform.position, target->transform.position, upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 8);
		break;
	}
	case trackingCam:
	{
		glm::mat4 tarView = glm::lookAt(transform.position, target->transform.position, upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 8);
		break;
	}
	case orbitTargetCam:
	{
		transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

		followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
		glm::vec3 tarPos = target->transform.position + followDistance;
		transform.position = transform.position + (tarPos - transform.position) * (float)(g_deltatime * 8);

		glm::mat4 tarView = glm::lookAt(transform.position, target->transform.position, upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 8);
		break;
	}
	case orbitPositionCam:
	{
		transform.rotation = glm::rotate(glm::mat4(1.0f), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed), rotationDirection);

		followDistance = transform.rotation * glm::vec4(followDistance, 1.0);
		glm::vec3 tarPos = lookatPosition + followDistance;
		transform.position = transform.position + (tarPos - transform.position) * (float)(g_deltatime * 8);

		glm::mat4 tarView = glm::lookAt(transform.position, lookatPosition, upDirection);
		view = view + (tarView - view) * (float)(g_deltatime * 8);
		break;
	}
	case firstPersonCam:
	{
		float x = (mousePos.x - lastX) / 100.0f;
		float y = (mousePos.y - lastY) / 100.0f;

		target->transform.yaw(-x);
		target->transform.pitch(y);

		lastX = mousePos.x;
		lastY = mousePos.y;

		transform.position = target->transform.position + target->transform.forward()*1.85f - target->transform.up()*0.1f;
		view = glm::lookAt(transform.position, target->transform.position + target->transform.forward()*5.0f, target->transform.up());
		break;
	}
	}
}

//	Camera keyboard inputs
void Camera::input(int key, int scancode, int action, int mods)
{
	//	Change Camera Mode
	int modeChange = 0;
	if ((key == GLFW_KEY_MINUS || key == GLFW_KEY_KP_SUBTRACT || key == GLFW_KEY_SLASH) && action == GLFW_PRESS)
		modeChange = 1;
	if (key == GLFW_KEY_RIGHT_SHIFT && action == GLFW_PRESS)
		modeChange = -1;

    if (modeChange != 0)
    {
        mode = static_cast<CameraMode>(int(mode) + modeChange);
		if (mode == staticCam)
		{
			transform.position = glm::vec3(0, 200, .1);
			lookatPosition = glm::vec3(0, -100, 0);
		}
		if (mode == followCam)
		{
			setFollowDistance(glm::vec3(0, 5, 10));
			target = glider;
		}
		if (mode == trackingCam)
		{
			transform.position = glm::vec3(0, 100, .1);
			target = glider;
		}
		if (int(mode) < 0 || int(mode) == 6)
		{
			mode = orbitTargetCam;
			followDistance = glm::vec3(0, 10, 20);
			target = glider;
		}
		if (mode == orbitPositionCam)
		{
			lookatPosition = glm::vec3(0, 0, 0);
			followDistance = glm::vec3(0, 50, 100);
		}
        if (int(mode) > 6 || int(mode) == 0)
        {
            mode = freeCam;
            transform.position = glm::vec3(0, 30, 0);
            //transform.rotation = glm::mat4(1.0f);
			lastX = mousePos.x;
			lastY = mousePos.y;
        }
        LOG_DEBUG("Cam mode: %d", int(mode));
    }


	//	FreeCam Movement Speed
	float speed = 120.0f * g_deltatime;

    if (mode == freeCam)
    {
		//	X-axis
		if (key == GLFW_KEY_J)
			transform.position += glm::vec3(speed, 0, 0);
		if (key == GLFW_KEY_L)
			transform.position += glm::vec3(-speed, 0, 0);

		//	Y-axis
		if (key == GLFW_KEY_Y)
			transform.position += glm::vec3(0, speed, 0);
		if (key == GLFW_KEY_H)
			transform.position += glm::vec3(0, -speed, 0);

		//	Z-axis
		if (key == GLFW_KEY_I)
			transform.position += glm::vec3(0, 0, speed);
		if (key == GLFW_KEY_K)
			transform.position += glm::vec3(0, 0, -speed);

		

		//	Zoom
		if (key == GLFW_KEY_N || key == GLFW_KEY_W)
			transform.position += transform.forward() * speed;
		if (key == GLFW_KEY_M || key == GLFW_KEY_S)
			transform.position -= transform.forward() * speed;

		//	Strafe
		if (key == GLFW_KEY_A)
			transform.position += transform.right() * speed;
		if (key == GLFW_KEY_D)
			transform.position -= transform.right() * speed;
    }
}

//	Update projection
void Camera::updateProjection(int width, int height)
{
	projection = glm::perspective(glm::radians(45.0f), float(width) / float(height), 0.01f, 1000.f);
}


//	Rotation Functions
void Camera::rotate(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    lookatDirection = transform.rotation * glm::vec4(lookatDirection, 1.0);
}

void Camera::tilt(glm::vec3 dir)
{
    transform.rotation = glm::rotate(glm::mat4(1.0), float(glm::radians(rotationAngle) * g_deltatime * rotationSpeed * 4), dir);
    upDirection = transform.rotation * glm::vec4(upDirection, 1.0);
}

//	Set variable functions
void Camera::setCamMode(CameraMode newMode)
{
    mode = newMode;
}

void Camera::setFollowDistance(glm::vec3 newDistance)
{
    followDistance = newDistance;
}

void Camera::setUpDirection(glm::vec3 newDirection)
{
    upDirection = newDirection;
}

void Camera::setLookatDirection(glm::vec3 newDirection)
{
    lookatDirection = newDirection;
}

void Camera::setLookatPosition(glm::vec3 newPosition)
{
    lookatPosition = newPosition;
}

void Camera::setTarget(GameObject* newTarget)
{
    target = newTarget;
}

void Camera::setRotationDirection(glm::vec3 newDirection)
{
    rotationDirection = newDirection;
}

void Camera::setRotationAngle(float angle)
{
    rotationAngle = angle;
}

void Camera::setRotationSpeed(float speed)
{
    rotationSpeed = speed;
}


//	Get variable functions
CameraMode Camera::getCamMode()
{
	return mode;
}

glm::vec3 Camera::getFollowDistance()
{
    return followDistance;
}

glm::vec3 Camera::getUpDirection()
{
    return upDirection;
}

glm::vec3 Camera::getLookatDirection()
{
    return lookatDirection;
}

glm::vec3 Camera::getLookatPosition()
{
    return lookatPosition;
}

glm::mat4 Camera::getProjection()
{
    return projection;
}

glm::mat4 Camera::getView()
{
    return view;
}

GameObject* Camera::getTarget()
{
    return target;
}
