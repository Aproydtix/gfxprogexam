#include "src/headers/GameObject.h"
#include "src/headers/mad.h"
#include "src/headers/Init.h"
#include "src/headers/Camera.h"
#include "src/headers/ObjectHandler.h"
#include "src/headers/Globals.h"

extern Camera* mainCamera;

extern int SCREEN_WIDTH;
extern int SCREEN_HEIGHT;


//	Create GameObject
GameObject::GameObject(glm::vec3 pos, glm::mat4 rot, glm::vec3 scale)
{
	transform.position = pos; // Position 0
	transform.rotation = rot; // Identity matrix
	transform.scale = scale; // Scale 1
}

//	Initializes everything for the GameObject to be updated and drawn automatically
void GameObject::init(Mesh* obj, Texture* tex, Shader* shade)
{
	gameObjects.push_back(this);

	mesh = new Mesh;
	mesh = obj;
	prevMesh = mesh;
	buffer = new Buffer;
	material.shader = shade;
	material.texture = tex;

    if (mesh != nullptr) setMeshBuffers();
}

//	Internal draw function that all GameObjects use
void GameObject::drawInternal(glm::mat4 view)
{
	//	Check whether GameObject has required components
	if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
	{
		//	Model
		glm::mat4 model(1.0f);
		model = glm::translate(model, transform.position);
		model *= transform.rotation;
		model = glm::scale(model, transform.scale);

		//	Common shader data
		material.shader->setMatrix("view", view);
		material.shader->setMatrix("model", model);
		material.shader->setMatrix("projection", mainCamera->getProjection());
		material.shader->setVec4("matColor", material.color);

		//	Check mesh for updates
		if (mesh != prevMesh)
		{
			setMeshBuffers();
			prevMesh = mesh;
		}
		else
			glBindVertexArray(buffer->VAO);	// Bind VAO for drawing

		//	Draw
		glBindTexture(GL_TEXTURE_2D, material.texture->m_texture_number);
		glDrawArrays(GL_TRIANGLES, 0, mesh->vertices.size() * sizeof(glm::vec3));
	}
}

//	General draw function, can also be overwritten by child classes
void GameObject::draw()
{
	//	Check whether GameObject has required components
    if (mesh != nullptr && material.shader != nullptr && material.texture != nullptr)
    {
		//	Standard Shader
        material.shader->setMatrix("view", mainCamera->getView());				
        material.shader->setVec3("lightPos", lights[0]->transform.position);	//	GET FROM GLOBAL LIGHT
        material.shader->setVec3("lightColor", lights[0]->color);				//	GET FROM GLOBAL LIGHT
        material.shader->setVec3("camPos", mainCamera->transform.position);		//	GET FROM GLOBAL CAMERA
        material.shader->setFloat("specularity", material.specularity);

		drawInternal(mainCamera->getView());
    }
}

void GameObject::update()
{
    // To be implemented by child classes
}

GameObject::~GameObject()
{
	//	Do not delete mesh, mesh points to globally saved meshes
	//		Same goes for material.shader, and texture

	delete buffer;
}

void GameObject::input(int key, int scancode, int action, int mods)
{
	//	To be implemented by child classes
}

//	Initial setting of buffers, also used to update if mesh has changed
void GameObject::setMeshBuffers()
{
	glGenVertexArrays(1, &buffer->VAO);                                         // Generate VAO with unique ID
	glBindVertexArray(buffer->VAO);                                             // Binds the VAO so changed to buffers will be stored

	glGenBuffers(3, &buffer->VBO[0]);                                           // Generates 3 unique ID for buffer object
	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[0]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->vertices.size() * sizeof(glm::vec3)), mesh->vertices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(0); // <-------------------------------- Remember

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[1]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->texCoord.size() * sizeof(glm::vec2)), mesh->texCoord.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ARRAY_BUFFER, buffer->VBO[2]);                              // Binds unique buffer to a target, GL_ARRAY_BUFFER here
	glBufferData(GL_ARRAY_BUFFER, (mesh->normals.size() * sizeof(glm::vec3)), mesh->normals.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);               // How to interpret the vertex data
																				// Which vertex attribute, number of dimensions in the vector, type of data, normalized or not, space between attribute sets, offset

	glEnableVertexAttribArray(2);
	
	// Unbinds everything
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}