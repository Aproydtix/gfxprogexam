# GFX Programming Exam
Made with OpenGL and C++.

Some of the basis for the project was co-developed before the exam, everything else was done by me in the span of 48 hours.
The framework consists of simple systems for 3D rendering, object loading, loading textures, music and sound, and organizing game objects in a scene.
I did most of the work for the overall systems for the framework, as well as the gameobject system.

The program generates a 3d mesh based on a provided heightmap, and is then colored based on height and season. There's a sun rotating around the terrain at different amplitudes based on season and giving off light based on position. A myriad of camera modes were also implemented, as well as other minor features, such as a glider, collectibles, shooting, and a HUD.

### Shaders and Textures
#### Shaders 
- The standard shader is used for the loaded texture with phong lighting model.
- The BaW shader loads the model with a black and white shader.
- The text shader is meant for text, but can also be used for other unlit materials
- The height shader is used to color terrain based on height

#### Texture 
- The program only supports ".png" formats. If another format is to be used, this needs to be changed in texture.cpp (in constructor for texture, GL_RGBA -> GL_RGB), and in gameObject.cpp (change ".png" in texture to desired format).


### Heightmap and Daytime

The heightmap loads from .png only, with a single color channel. I admittedly got a fair bit of help creating the 3D Mesh from this data.

Seasons and daytime change as specified in the task.
In addition, daylight changes depending on the season, as does the sun's position.
There is a smooth transition between both seasons and daytime, and other variables such as vegetation color and water level also change with the season.
Geometry on the heightmap below water level is not rendered.


### Glider

The glider has two input modes, "physics" and "arcade", you can change mode with P.
You can reset its position with R, and randomize it with F.
Roll is controlled with A and D.
Pitch with W and S,
and Yaw with Q and E.
The left mouse-button and space allow you to shoot bullets to hit balloons.
The glider admittedly doesn't work very well, in large part due to limitations caused by the input system.


### Camera modes

The camera mode can be changed by pressing "-" or Right-Shift.
Pressing Escape releases or locks the mouse cursor.

- freeCam: free movement of the camera from input (as specified by the exam, WASD also gives input).
- staticCam: camera has static position, looks at entire map.
- orbitPositionCam: camera orbits a static position, shows entire map.
- trackingCam: camera follows the glider, but has static position.
- followCam: camera follows behind target (glider), upDirection is locked to world up.
- firstPersonCam: camera is set inside cockpit, and both position and rotation is locked to the glider.
- orbitTargetCam: camera orbits target (game object) in a distance equals followDistance


# Framework

The framework was made with the collaboration of Maarten Dijkstra, Eirik Hiis Hauge, Haakon Aarstein, Sindre Blomberg, Nataniel Gåsøy, Herman Tandberg Dybing and Christian Bråthen Tverberg.


## Build and run

Build instructions used are assimilated from the lab instructions for this course.

#### Windows (cmake-gui)

1. Set the source code file to the root directory containing "CMakeLists.txt"
2. Binaries can be built in the directory of your choice, e.g.~"OpenGL\build\"
3. Click the configure button and select which generator you would like to use
4. Click the generate button
5. If your generator is an IDE such as Visual Studio, then open up the newly created .sln file and build "ALL_BUILD". After this you might want to set "main" as you StartUp Project.

#### Linux (Ubuntu)

- Create build subdirectory in directory and navigate into it
- Generate UNIX Makefile (point CMake to CMakeLists.txt)
  cmake ..
- Execute make command
  make
- Run executable
  ./bin/main

## Video Demo
https://youtu.be/AM5RC_AfDdM

## Screenshots
![Screenshot1](Screenshot1.png)
![Screenshot2](Screenshot2.png)
![Screenshot3](Screenshot3.png)