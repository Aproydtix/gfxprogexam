#version 430 core

in vec2 TexCoord;
in vec3 fragNormal;
in vec3 fragVert;

out vec4 FragColor;

uniform vec3 camPos;
uniform vec3 lightPos;
uniform mat4 model;
uniform sampler2D ourTexture;
uniform vec3 lightColor;
uniform vec4 matColor;
uniform float specularity;

uniform int hardLines;
uniform float minH;
uniform float maxH;
uniform vec4 waterColor;		// = vec4(.25f, .5f, 1f, 1f);
uniform vec4 vegetationColor;	// = vec4(.2f, 1.0f, .2f, 1f);
uniform vec4 mountainColor;		// = vec4(.45f, .45f, .1f, 1f);
uniform vec4 snowColor;			// = vec4(.8f, .9f, 1f, 1f);

void main() {
	// Invisible if alhpa is low
	vec4 tex = texture(ourTexture, TexCoord) * matColor;
	if(tex.a < 0.1)
		discard;

	vec3 fragPosition = vec3(model * vec4(fragVert, 1.0));
	float pos = fragPosition.y;
	if (pos < minH * 1.05f)
		discard;

	// Ambient
	float ambientStrength = 0.3;
	vec3 ambient = (ambientStrength * vec3(1.0f, 1.0f, 1.0f) + ambientStrength * lightColor)/2f; 


	// Diffuse
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);
	
	vec3 surfaceToLight = normalize(lightPos - fragPosition);

	float brightness = max(dot(normal, surfaceToLight), 0.1);
	vec3 diffuse = brightness * lightColor;


	// specular
	vec3 viewDir = normalize(camPos - fragVert);
	vec3 reflectDir = reflect(-surfaceToLight, normal);
	float spec = 0.0;
	vec3 halfwayDir = normalize(surfaceToLight + viewDir);  
	spec = pow(max(dot(normal, halfwayDir), 0.0), specularity);
	vec3 specular = spec * lightColor;    

	float dist = distance(lightPos, fragVert)/1000.0f;
	float attenuation = 1.0f / (1.0 + (0.03125 * dist));

	// final colors
	vec4 surfaceColor;

	float midH = (2*minH + maxH)/3.0f;

	if (pos < minH)
	{
		surfaceColor = waterColor;
	}
	else if (pos < midH)
	{	
		float t = 0;
		if (hardLines == 0)
			t = pow((pos-minH)/(midH-minH), 2);
		surfaceColor = (1.0f-t) * vegetationColor + t * mountainColor;
	}
	else if (pos < maxH)
	{
		float t = 0;
		if (hardLines == 0)
			t = pow((pos-midH)/(maxH-midH), 4);
		surfaceColor = (1.0f-t) * mountainColor + t * snowColor;
	}
	else 
	{
		surfaceColor = snowColor;
	}

	surfaceColor = (2.0f*surfaceColor + tex)/3.0f;

	FragColor = vec4((ambient + diffuse) * surfaceColor.rgb + (specular/8.0f), 1);
}