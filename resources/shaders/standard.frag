#version 430 core

in vec2 TexCoord;
in vec3 fragNormal;
in vec3 fragVert;

out vec4 FragColor;

uniform vec3 camPos;
uniform vec3 lightPos;
uniform mat4 model;
uniform sampler2D ourTexture;
uniform vec3 lightColor;
uniform vec4 matColor;
uniform float specularity;

void main() {
	// Invisible if alhpa is low
	vec4 tex = texture(ourTexture, TexCoord) * matColor;
	if(tex.a < 0.1)
		discard;

	// Ambient
	float ambientStrength = 0.3;
	vec3 ambient = (ambientStrength * vec3(1.0f, 1.0f, 1.0f) + ambientStrength * lightColor)/2f;


	// Diffuse
	mat3 normalMatrix = transpose(inverse(mat3(model)));
	vec3 normal = normalize(normalMatrix * fragNormal);
	
	vec3 fragPosition = vec3(model * vec4(fragVert, 1.0));
	vec3 surfaceToLight = normalize(lightPos - fragPosition);

	float brightness = max(dot(normal, surfaceToLight), 0.1);
	vec3 diffuse = brightness * lightColor;


	// specular
	vec3 viewDir = normalize(camPos - fragVert);
	vec3 reflectDir = reflect(-surfaceToLight, normal);
	float spec = 0.0;
	vec3 halfwayDir = normalize(surfaceToLight + viewDir);  
	spec = pow(max(dot(normal, halfwayDir), 0.0), specularity);
	vec3 specular = spec * lightColor;    
	
	float dist = distance(lightPos, fragVert)/10.0f;
	float attenuation = 1.0f / (1.0 + (0.03125 * dist));

	// final colors
	vec4 surfaceColor = texture(ourTexture, TexCoord) * matColor;
	FragColor = vec4((ambient + diffuse) * surfaceColor.rgb + (specular/1.5f), 1);
}